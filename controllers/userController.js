const User = require('./../models/userModels');
exports.getAllUsers = async(req,res,next) =>{
    try{ 
        const users = await User.find()
        res.status(200).json({data: users,status:'success'})
    } catch(err){
        res.status(500).json({error:err.messge});
    }}
    
exports.createUser = async(req,res) =>{ 
    try{
        const user = await User.create(req.body);
        console.log(req.body.name)
        res.json({data: user,status:'success'})
    } catch(err){
        res.status(500).json({error:err.messge});
    }}
    
exports.getUser = async(req,res) =>{ 
        try{
            const user = await User.findById(req.params.id);
            res.json({data: user,status:'success'})
        } catch(err){ 
            res.status(500).json({error:err.messge}); 
        }}

exports.updateUser = async(req,res) =>{
    try{
        const user = await User.findByIdandupdate(req.params.id,req.body);
        res.json({data: Users,status:'success'})
    } catch(err){ 
        res.status(500).json({error:err.messge});
    }}
    
exports.deleteUser = async(req,res) =>{
    try{
        const Users = await User.findByIdAndDelete(req.params.id);
        res.json({data: Users,status:'success'})
    } catch(err){
        res.status(500).json({error:err.messge});  
    }}